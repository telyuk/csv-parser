import csv
import uuid
import textwrap


class CSVParser:

    def __init__(self, model_name):
        self.model_name = model_name

    def parse(self):
        with open("path_to_csv_file", "r") as csv_file, open("path_to_migration_file", "r+") as migration_file:
            header = next(csv.reader(csv_file))
            field_names = [key.replace(" ", "").lower() for key in header]
            deepest_level = max((int(key[-1]) for key in field_names if key.startswith("level")))
            reader = csv.DictReader(csv_file, fieldnames=field_names)
            last_level_instance_uuid_mapping = {}
            content = migration_file.readlines()
            separate_index = content.index("class Migration(migrations.Migration):\n")
            header_content = content[:separate_index + 2]
            footer_content = content[separate_index + 2:]
            self._fill_header(file=migration_file, header_content=header_content)
            for line in reader:
                key, title = next((key, title) for key, title in line.items()
                                  if key.startswith("level") and title != "")
                level = int(key[-1])
                pk = str(uuid.uuid4())
                if level < deepest_level:
                    last_level_instance_uuid_mapping.update(
                        {f"last_level_{level}_instance_uuid": pk}
                    )

                if level == 0:
                    row = self._build_row(pk=pk, level=level, code=line["code"], title=title)
                else:
                    parent_uuid = last_level_instance_uuid_mapping.get(f"last_level_{level - 1}_instance_uuid")
                    row = self._build_row(pk=pk, level=level, code=line["code"], title=title, parent_uuid=parent_uuid)
                migration_file.write("{row},\n".format(row=textwrap.indent(text=row, prefix=" " * 12)))
            self._fill_footer(file=migration_file, footer_content=footer_content)

    def _build_row(self, pk, level, code, title, parent_uuid=None):
        if not parent_uuid:
            result = f"""{self.model_name}(pk="{pk}", title="{title}", level={level}, code="{code}")"""
        else:
            result = f"""{self.model_name}(pk="{pk}", title="{title}", level={level}, code="{code}", parent_id="{parent_uuid}")"""

        return result

    def _fill_header(self, file, header_content):
        file.seek(0)
        file.writelines(header_content)
        file.write(textwrap.indent(text=f"""def forward(apps, schema_editor):\n""", prefix=" " * 4))
        file.write(textwrap.indent(text=f"""{self.model_name} = apps.get_model("core", "{self.model_name}")\n""", prefix=" " * 8))
        file.write(textwrap.indent(text="objs = [\n", prefix=" " * 8))

    def _fill_footer(self, file, footer_content):
        file.write(textwrap.indent(text="]\n", prefix=" " * 8))
        file.write(textwrap.indent(text=f"{self.model_name}.objects.bulk_create(objs=objs, batch_size=1000)\n\n", prefix=" " * 8))

        file.write(textwrap.indent(text="def backward(apps, schema_editor):\n", prefix=" " * 4))
        file.write(textwrap.indent(text=f"""{self.model_name} = apps.get_model("core", "{self.model_name}")\n""", prefix=" " * 8))
        file.write(textwrap.indent(text=f"{self.model_name}.objects.all().delete()\n\n", prefix=" " * 8))

        for idx, line in enumerate(footer_content):
            if "\'" in line:
                footer_content.insert(idx, footer_content.pop(idx).replace("\'", "\""))
            if "operations = [" in line:
                footer_content.insert(
                    idx + 1,
                    textwrap.indent(text="migrations.RunPython(forward, backward),\n", prefix=" " * 8)
                )
        file.writelines(footer_content)


if __name__ == "__main__":
    CSVParser(model_name="model_name").parse()
