1. Create the data migration file:
```bash
docker-compose exec backend python manage.py makemigrations --name <data_migration_name> <app_name> --empty
```
2. Enter the path to the data migration file into the parse script `path_to_migration_file`
3. Enter the path to the CSV file into the parse script `path_to_csv_file`
4. Pass the model name to CSVParser `model_name` 
5. Run script
